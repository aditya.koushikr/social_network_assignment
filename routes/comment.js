var express=require("express");
var router=express.Router({mergeParams: true});

var connection=require("../DBConnect");

router.get("/getComment",function(req,res)
{
    
     connection.query('select * from comments ', function(err,rows)
    {       
        if(err)
        {
            console.log(err);
        }
        else{
            res.json(rows);
        }
    });
});

router.get("/",function(req,res)
{
    console.log(req.params);
     connection.query('select * from comments where id=?', req.params.id, function(err,rows)
    {       
        if(err)
        {
            console.log(err);
        }
        else{
            res.json(rows);
        }
    });
});


router.get("/:id",function(req,res)
{
    console.log(req.params.id)
     connection.query('select user_id,post_id,description from comments where id = ?',req.params.id,function(err,row)
    {
        if(err)throw err;
        else{
            res.json(row);
        }
    });
});

router.post("/addComment",function(req,res)
{
    console.log("hello");
    console.log(req.body);
     var user=req.body;
    
    connection.query(`insert into comments (user_id,post_id, description,created_at,updated_at ) values(
        ?,
        ?,
        ?,
        ?,
        ?)`,
        [user.user_id,
        user.post_id,
        user.description,
        getDateTime(user.created_at),
        getDateTime(user.updated_at)],function(err,count)
    {
        if(err) throw err;
        else{
            res.json(req.body);
            
        }
    });

    function getDateTime(datetime)
    {
        datetime=new Date();
        date.format(datetime,'YYYY/MM/DD HH:mm:ss');
        return datetime;
    }
});

router.put("/updateComment/:id",function(req,res)
{
    console.log(req.params.id);
    connection.query(`update comments set description=?,updated_at=? where id=?`,
        [req.body.description,
        getDateTime(req.body.updated_at),
        req.params.id],function(err)
    {
        if(err) throw err;
        else{
            console.log(req.body);
        }
    });

    function getDateTime(datetime)
    {
        date.format(datetime,'YYYY/MM/DD HH:mm:ss');
        return datetime;
    }
});

router.delete("/deleteComment/:id",function(req,res)
{
    console.log(req.params.id);
    connection.query(`delete from comments where id=?`,req.params.id);
})

module.exports.router=router;