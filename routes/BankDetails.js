var express=require("express");
var router=express.Router({mergeParams: true});
var crypto=require("crypto");
var connection=require("../DBConnect");
var date = require('date-and-time');

router.get("/getBankDetails",function(req,res)
{
     connection.query('select * from  bank_detail',function(err,rows)
    {       
        if(err)
        {
            console.log(err);
        }
        else{
            rows.forEach(function(row) {
             row.account_number=decrypt(row.account_number);   
            });
            res.json(rows);
        }
    });
    function decrypt(text){
  var decipher = crypto.createDecipher('aes-256-ctr','d6F3Efeq');
  var dec = decipher.update(text,'hex','utf8');
  dec += decipher.final('utf8');
  return dec;
    }
});

router.get("/",function(req,res)
{
    console.log(req.params);
     connection.query('select * from  bank_detail where id=?',req.params.id,function(err,rows)
    {       
        if(err)
        {
            console.log(err);
        }
        else{
            rows.forEach(function(row) {
             row.account_number=decrypt(row.account_number);   
            });
            res.json(rows);
        }
    });
    function decrypt(text){
  var decipher = crypto.createDecipher('aes-256-ctr','d6F3Efeq')
  var dec = decipher.update(text,'hex','utf8')
  dec += decipher.final('utf8');
  return dec;
    }
});


router.get("/:id",function(req,res)
{
    console.log(req.params.id)
     connection.query('select id,account_number,ifsc from bank_detail where id = ?',req.params.id,function(err,rows)
    {
        if(err)throw err;
        else{
            rows.forEach(function(row)
            {
                row.account_number=decrypt(row.account_number);
            });
            res.json(rows);
        }
    });
     function decrypt(text){
  var decipher = crypto.createDecipher('aes-256-ctr','d6F3Efeq');
  var dec = decipher.update(text,'hex','utf8');
  dec += decipher.final('utf8');
  return dec;
     }
});

router.post("/addBankDetails",function(req,res)
{
    console.log("hello");
    console.log(req.body);
     var user=req.body;
    
    connection.query(`insert into bank_detail (user_id,account_number,ifsc,created_at,updated_at ) values(
        ?,
        ?,
        ?,
        ?,
        ?)`,
        [user.user_id,
        encrypt(user.account_number),
        user.ifsc,
        getDate(user.created_at),
        user.updated_at],function(err,count)
    {
        if(err) throw err;
        else{
            res.json(req.body);
            
        }
    });
    function encrypt(text){
  var cipher = crypto.createCipher('aes-256-ctr','d6F3Efeq')
  var crypted = cipher.update(text,'utf8','hex')
  crypted += cipher.final('hex');
  return crypted;
}
function getDate(now)
    {
        now =new Date();
        date.format(now,'YYYY/MM/DD HH:mm:ss');
        return now;
    }
});

router.put("/updateBankDetails/:id",function(req,res)
{
    console.log(req.params.id);
    connection.query(`update bank_detail set account_number=?,ifsc=?,updated_at=? where id=?`,
        [encrypt(req.body.account_number),
        req.body.ifsc,
        getDate(req.body.updated_at),
        req.params.id],function(err)
    {
        if(err) throw err;
        else{
            console.log(req.body);
        }
    });
    function encrypt(text){
  var cipher = crypto.createCipher('aes-256-ctr','d6F3Efeq')
  var crypted = cipher.update(text,'utf8','hex')
  crypted += cipher.final('hex');
  return crypted;
    
    }
    function getDate(now)
    {
        now =new Date();
        date.format(now,'YYYY/MM/DD HH:mm:ss');
        return now;
    }
});

router.delete("/deleteBankDetails/:id",function(req,res)
{
    console.log(req.params.id);
    connection.query(`delete from bank_detail where id=?`,req.params.id);
})

module.exports.router=router;