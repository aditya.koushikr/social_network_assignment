var express=require("express");

var router=express.Router({mergeParams: true});
var date=require("date-and-time");
var connection=require("../DBConnect");

router.get("/getEvent",function(req,res)
{
    
     connection.query('select * from event_detail',function(err,rows)
    {       
        if(err)
        {
            console.log(err);
        }
        else{
            res.json(rows);
        }
    });
});


router.get("/",function(req,res)
{
    
     connection.query('select * from event_detail where id=?', req.params.id,function(err,rows)
    {       
        if(err)
        {
            console.log(err);
        }
        else{
            res.json(rows);
        }
    });
});


router.get("/:id",function(req,res)
{
    console.log(req.params.id)
     connection.query('select name,description,start_time,end_time from event_detail where id = ?',req.params.id,function(err,rows)
    {
        if(err)throw err;
        else{
            res.json(rows);
        }
    });
});

router.post("/addEvent",function(req,res)
{
    console.log("hello");
    console.log(req.body);
     var user=req.body;
    
    connection.query(`insert into event_detail (user_id, name, description,start_time,end_time,created_at,updated_at ) values(
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?)`,
        [user.user_id,
        user.name,
        user.description,
        getTime(user.start_time),
        getTime(user.end_time),
        getTime(getuser.created_at),
        user.updated_at],function(err,count)
    {
        if(err) throw err;
        else{
            res.json(req.body);
        }
    });
    
    function getDateTime(dateTime)
    {
        dateTime=new Date();
        date.format(dateTime,'YYYY/MM/DD HH:mm:ss');
        return now;
    }
});

router.put("/updateEvent/:id",function(req,res)
{
    console.log(req.params.id);
    
    connection.query(`update event_detail set name=?,description=?,start_time=?,end_time=?,updated_at=? where id=?`,
        [req.body.name,
        req.body.description,
        getDateTime(req.body.start_time),
        getDateTime(req.body.end_time),
        getDateTime(req.body.updated_at),
        req.params.id],function(err)
    {
        if(err) throw err;
        else{
            console.log(req.body);
        }
    });

    function getDateTime(dateTime)
    {
        dateTime=new Date();
        date.format(dateTime,'YYYY/MM/DD HH:mm:ss');
        return dateTime;
    }
});

router.delete("/deleteEvent/:id",function(req,res)
{
    
    connection.query(`delete from event_detail where id=?`,req.params.id);
})

module.exports.router=router;