var express=require("express");

var router=express.Router({mergeParams: true});

var connection=require("../DBConnect");

router.get("/getSocial",function(req,res)
{
    
     connection.query('select * from social_detail',  function(err,rows)
    {       
        if(err)
        {
            console.log(err);
        }
        else{
            res.json(rows);
        }
    });
});

router.get("/",function(req,res)
{
    console.log(req.params);
     connection.query('select * from social_detail where id=?',req.params.id,  function(err,rows)
    {       
        if(err)
        {
            console.log(err);
        }
        else{
            res.json(rows);
        }
    });
});


router.get("/:id",function(req,res)
{
    console.log(req.params.id);
     connection.query('select name,url from social_detail where id = ?',req.params.id,function(err,row)
    {
        if(err)throw err;
        else{
            res.json(row);
        }
    });
});

router.post("/addSocial",function(req,res)
{
    console.log("hello");
    console.log(req.body);
     var user=req.body;
    
    connection.query(`insert into social_detail (user_id, name, url ) values(
        ?,
        ?,
        ?)`,
        [user.user_id,
        user.name,
        user.url],function(err,count)
    {
        if(err) throw err;
        else{
            res.json(req.body);
        }
    });
});

router.put("/updateSocial/:id",function(req,res)
{
    console.log(req.params.id)
    var user=req.body;
    connection.query(`update social_detail set name=?,url=? where id=?`,
        [req.body.name,
        req.body.url,req.params.id],function(err)
    {
        if(err) throw err;
        else{
            console.log(req.body);
        }
    });
});

router.delete("/deleteSocial/:id",function(req,res)
{
    console.log(req.params.id);
    
    connection.query(`delete from social_detail where id=?`,req.params.id);
})

module.exports.router=router;