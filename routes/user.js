var express=require("express"); 
var router=express.Router({mergeParams: true});
var crypto=require("crypto");
var connection=require("../DBConnect");
var date=require("date-and-time");

router.get("/",function(req,res)
{
    
     connection.query('select * from user', req.params.id, function(err,rows)
    {       
        if(err)
        {
            console.log(err);
        }
        else{
            rows.forEach(function(row)
            {
                row.password=decrypt(row.password);
            })
            res.json(rows);
        }
    });

    function decrypt(text)
    {
        var decipher=crypto.createDecipher('aes-256-ctr','d62juqie2');
        var dec=decipher.update(text,'hex','utf8');
        dec+=decipher.final('utf8');
        return dec;
    }
});


router.get("/:id",function(req,res)
{
    console.log(req.params.id);
     connection.query('select user_name,password,first_name,last_name,email from user where id = ?',req.params.id,function(err,rows)
    {
        if(err)throw err;
        else{
            rows.forEach(function(row)
            {
                row.password=decrypt(row.password);
            });
            res.json(rows);
        }
    });

    function decrypt(text)
    {
        var decipher=crypto.createDecipher('aes-256-ctr','d62juqie2');
        
        var decrypted=decipher.update(text,'hex','utf8');
        console.log(decrypted);
        decrypted+=decipher.final('utf8');
        return decrypted;
    }
});



router.post("/",function(req,res)
{
    console.log("hello");
    console.log(req.body);
     var user=req.body;
    
    connection.query(`insert into user (user_name, password, first_name, last_name, email, phone_number, gender, date_of_birth, created_at, updated_at ) values(
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?)`,
        [user.user_name,
        encrypt(user.password),
        user.first_name,
        user.last_name,
        user.email,
        user.phone_number,
        user.gender,
        user.date_of_birth,
        getDate(user.created_at),
        user.updated_at],function(err,count)
    {
        if(err) throw err;
        else{
            res.json(req.body);
        }
    });
    function encrypt(text)
    {
        var cipher=crypto.createCipher('aes-256-ctr','d62juqie2');
        var crypted=cipher.update(text,'utf8','hex');
        crypted+=cipher.final('hex');
        return crypted;
    }

    function getDate(now)
    {
        now=new Date();
        date.format(now,'YYYY/MM/DD HH:mm:ss');
        return now;
    }
});

router.put("/:id",function(req,res)
{
    console.log(req.params.id);
    //var user=req.body;
    connection.query(`update user set user_name=?,password=?,first_name=?,last_name=?,email=?,phone_number=?,gender=?,date_of_birth=?,updated_at=? where id=?`,
        [req.body.user_name,
        encrypt(req.body.password),
        req.body.first_name,
        req.body.last_name,
        req.body.email,
        req.body.phone_number,
        req.body.gender,
        req.body.date_of_birth,        
        getDate(req.body.updated_at),
        req.params.id],function(err)
    {
        if(err) throw err;
        else{
            console.log(req.body);
        }
    });
    function encrypt(text)
    {
        var cipher=crypto.createCipher('aes-256-ctr','d62juqie2');
        var crypted=cipher.update(text,'utf8','hex');
        crypted+=cipher.final('hex');
        return crypted;
    }
    
    function getDate(now)
    {
        now=new Date();
        date.format(now,'YYYY/MM/DD HH:mm:ss');
        return now;
    }
});

router.delete("/:id",function(req,res)
{
    var user=req.body;
    console.log(req.params.id)
    connection.query(`delete from user where id=?`,req.params.id,function(err)
    {
        if(err) throw err;
        else{
            console.log("deleted the row successfylly!!!");
        }
    });
})

module.exports.router=router;