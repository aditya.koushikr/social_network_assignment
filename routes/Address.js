var express=require("express");
var router=express.Router({mergeParams: true});
var date=require("date-and-time");
var connection=require("../DBConnect");


router.get("/getAddress",function(req,res)
{
    
     connection.query('select * from address',function(err,rows)
    {       
        if(err)
        {
            console.log(err);
        }
        else{
            res.json(rows);
        }
    });
});

router.get("/",function(req,res)
{
    console.log(req.params);
     connection.query('select * from address where id=?',req.params.id,function(err,rows)
    {       
        if(err)
        {
            console.log(err);
        }
        else{
            res.json(rows);
        }
    });
});


router.get("/:id",function(req,res)
{
    console.log(req.params.id);
     connection.query('select user_id from address where id = ?',req.params.id,function(err,row)
    {
        if(err)throw err;
        else{
            res.json(row);
        }
    });
});

router.post("/new",function(req,res)
{
    console.log("hello");
    console.log(req.body);
     var user=req.body;
    
    connection.query(`insert into address (user_id, street_1, street_2, LandMark, city, state, zip_code, country, created_at, updated_at ) values(
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?)`,
        [user.user_id,
        user.street_1,
        user.street_2,
        user.LandMark,
        user.city,
        user.state,
        user.zip_code,
        user.country,
        getDate(user.created_at),
        user.updated_at],function(err,count)
    {
        if(err) throw err;
        else{
            res.json(req.body);
        }
    });

    function getDate(now)
    {
        now=new Date();
        date.format(now,'YYYY/MM/DD HH:mm:ss');
        return now;
    }
});

router.put("/updateAddress/:id",function(req,res)
{
    console.log(req.params.id);
    
    connection.query(`update address set  street_1=?, street_2=?, LandMark=?, city=?, state=?, zip_code=?, country=?, updated_at=? where id=?`,
        [req.body.street_1,
        req.body.street_2,
        req.body.LandMark,
        req.body.city,
        req.body.state,
        req.body.zip_code,
        req.body.country,
        getDate(req.body.updated_at),req.params.id],function(err)
    {
        if(err) throw err;
        else{
            console.log(req.body);
        }
    });

    function getDate(now)
    {
        now=new Date();
        date.format(now,'YYYY/MM/DD HH:mm:ss');
        return now;
    }
});

router.delete("/deleteAddress/:id",function(req,res)
{
    
    connection.query(`delete from address where id=?`,req.params.id);
})

module.exports.router=router;