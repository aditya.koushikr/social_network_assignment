var express=require("express");
var app=express();
var bodyparser=require("body-parser");

app.use(bodyparser.urlencoded({extended:true}));
app.use(bodyparser.json());
app.use("/users",require('./routes/user').router);
app.use("/address",require('./routes/Address').router);
app.use("/social",require('./routes/social').router);
app.use("/events",require('./routes/Events').router);
app.use("/post",require('./routes/post').router);
app.use("/comment",require('./routes/comment').router);
app.use("/friends",require('./routes/FriendRequest').router);
app.use("/account_info",require('./routes/BankDetails').router);
app.use("/location",require('./routes/location').router);


app.use("/users/:id/address",require("./routes/Address").router);
app.use("/users/:id/social_info",require("./routes/social").router);
app.use("/users/:id/event_info",require("./routes/Events").router);
app.use("/users/:id/post_info",require("./routes/post").router);
app.use("/users/:id/comment_info",require("./routes/comment").router);
app.use("/users/:id/friend_info",require("./routes/FriendRequest").router);
app.use("/users/:id/account_info",require("./routes/BankDetails").router);
app.use("/users/:id/location_info",require("./routes/location").router);
var port=process.env.PORT||3000;




app.listen(port,function(err)
{
    if(err)
    {
        console.log(err);
    }
    else{
        console.log("server is running at the port: "+port);
    }
});


