var express=require("express");
var router=express.Router({mergeParams: true});

var connection=require("../DBConnect");

router.get("/getFriends",function(req,res)
{
    
     connection.query('select * from friend_list', function(err,rows)
    {       
        if(err)
        {
            console.log(err);
        }
        else{
            res.json(rows);
        }
    });
});

router.get("/",function(req,res)
{
    console.log(req.params);    
     connection.query('select * from friend_list where id=?', req.params.id, function(err,rows)
    {       
        if(err)
        {
            console.log(err);
        }
        else{
            res.json(rows);
        }
    });
});


router.get("/:id",function(req,res)
{
    console.log(req.params.id)
     connection.query('select status from friend_list where id = ?',req.params.id,function(err,row)
    {
        if(err)throw err;
        else{
            res.json(row);
        }
    });
});

router.post("/addFriend",function(req,res)
{
    console.log("hello");
    console.log(req.body);
     var user=req.body;
    
    connection.query(`insert into friend_list (user_sent,user_received,status,created_at,updated_at ) values(
        ?,
        ?,
        ?,
        ?,
        ?)`,
        [user.user_sent,
        user.user_received,
        user.status,
        getDate(user.created_at),
        getDate(user.updated_at)],function(err,count)
    {
        if(err) throw err;
        else{
            res.json(req.body);
            
        }
    });

    function getDate(now)
    {
        now=new Date();
        date.format(now,'YYYY/MM/DD HH:mm:ss');
        return now;
    }
});

router.put("/updateFriend/:id",function(req,res)
{
    console.log(req.params.id);
    connection.query(`update friend_list set user_sent=?,user_received=?,status=?,updated_at=? where id=?`,
        [req.body.user_sent,
        req.body.user_received,
        req.body.status,
        getDate(req.body.updated_at),
        req.params.id],function(err)
    {
        if(err) throw err;
        else{
            console.log(req.body);
        }
    });


    function getDate(now){
        now=new Date();
        date.format(now,'YYYY/MM/DD HH:mm:ss');
        return now;
    }
});

router.delete("/deleteFriend/:id",function(req,res)
{
    console.log(req.params.id);
    connection.query(`delete from friend_list where id=?`,req.params.id);
})

module.exports.router=router;