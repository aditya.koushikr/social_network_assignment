var express=require("express");
var router=express.Router({mergeParams: true});
var date=require('date-and-time');
var connection=require("../DBConnect");

router.get("/getLocation",function(req,res)
{
    
     connection.query('select * from location', function(err,rows)
    {       
        if(err)
        {
            console.log(err);
        }
        else{
            res.json(rows);
        }
    });
});

router.get("/",function(req,res)
{
    console.log(req.params);
     connection.query('select * from location where id=?',req.params.id,  function(err,rows)
    {       
        if(err)
        {
            console.log(err);
        }
        else{
            res.json(rows);
        }
    });
});


router.get("/:id",function(req,res)
{
    console.log(req.params.id)
     connection.query('select name,lat,lon,type from location where id = ?',req.params.id,function(err,row)
    {
        if(err)throw err;
        else{
            res.json(row);
        }
    });
});

router.post("/addLocation",function(req,res)
{
    console.log("hello");
    console.log(req.body);
     var user=req.body;
    
    connection.query(`insert into location (name,lat,lon,post_id,event_id,type,created_at,updated_at) values(
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?)`,
        [user.name,
        user.lat,
        user.lon,
        user.post_id,
        user.event_id,
        user.type,
        getDate(user.created_at),
        user.updated_at],function(err,count)
    {
        if(err) throw err;
        else{
            res.json(req.body);
            
        }
    });
    function getDate(now)
    {
        now =new Date();
        date.format(now,'YYYY/MM/DD HH:mm:ss');
        return now;
    }
});

router.put("/updateLocation/:id",function(req,res)
{
    console.log(req.params.id);
    connection.query(`update location set name=?,lat=?,lon=?,post_id=?,event_id=?,type=?,updated_at=? where id=?`,
        [req.body.name,
        req.body.lat,
        req.body.lon,
        req.body.post_id,
        req.body.event_id,
        req.body.type,
        getDate(req.body.updated_at),
        req.params.id],function(err)
    {
        if(err) throw err;
        else{
            console.log(req.body);
        }
    });
    function getDate(now)
    {
        now =new Date();
        date.format(now,'YYYY/MM/DD HH:mm:ss');
        return now;
    }
});

router.delete("/deleteLocation/:id",function(req,res)
{
    console.log(req.params.id);
    connection.query(`delete from location where id=?`,req.params.id);
})

module.exports.router=router;