var express=require("express");
var router=express.Router({mergeParams: true});

var connection=require("../DBConnect");

router.get("/getPost",function(req,res)
{
    
     connection.query('select * from post',function(err,rows)
    {       
        if(err)
        {
            console.log(err);
        }
        else{
            res.json(rows);
        }
    });
});

router.get("/",function(req,res)
{
    
     connection.query('select * from post where id=?',  req.params.id,function(err,rows)
    {       
        if(err)
        {
            console.log(err);
        }
        else{
            res.json(rows);
        }
    });
});


router.get("/:id",function(req,res)
{
    console.log(req.params.id)
     connection.query('select description,tag_friends from post where id = ?',req.params.id,function(err,row)
    {
        if(err)throw err;
        else{
            res.json(row);
        }
    });
});

router.post("/addPost",function(req,res)
{
    console.log("hello");
    console.log(req.body);
     var user=req.body;
    
    connection.query(`insert into post (user_id, description,tag_friends ) values(
        ?,
        ?,
        ?)`,
        [user.user_id,
        user.description,
        user.tag_friends]
        ,function(err,count)
    {
        if(err) throw err;
        else{
            res.json(req.body);
            
        }
    });
});

router.put("/updatePost/:id",function(req,res)
{
    console.log(req.params.id);
    connection.query(`update post set description=?,tag_friends=? where id=?`,
        [req.body.description,
        req.body.tag_friends,
        req.params.id],function(err)
    {
        if(err) throw err;
        else{
            console.log(req.body);
        }
    });
});

router.delete("/deletePost/:id",function(req,res)
{
    console.log(req.params.id);
    connection.query(`delete from post where id=?`,req.params.id);
})

module.exports.router=router;